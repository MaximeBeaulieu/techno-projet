import React from 'react';
import { View,Text,Button, Share,ScrollView,Linking,StyleSheet } from 'react-native';
numero1="0000000000";
numero2="0000000000";
numero3="0000000000";
numero4="0610947770";
numero5="0000000000";
numero6="0000000000";
numero7="0000000000";
numero8="0000000000";


function MembresScreen() {
    return (
        <ScrollView>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text  style={{ fontSize: 24, textAlign: "center",fontStyle: 'italic',fontWeight: 'bold'}}>Liste et contact des différents membres</Text>
            {/* <Text  style={styles.bigblue}>Liste et contact des différents membres</Text> */}
            <Text  style={{ fontSize: 14, textAlign: "center",fontStyle: 'italic'}}>Vous trouverez ci-dessous une liste des différents membres, au clic sur le premier bouton, vous obtiendrez une fiche contact partageable (avec le nom et prénom de la personne et son numéro de téléphone). Au clic sur le bouton "Appeler", vous pourrez appeler le numéro correspondant à la personne que vous souhaitez.</Text>
            <Text>
                {"\n"}
            </Text>
            <Button
                title="Présidente"
                onPress={()=>{
                    Share.share({message:'Romane Parès 0000000000'})
                }}
                color="#841584"
                accessibilityLabel="Learn more about BDE"
            />
            
            <Button 
                title="Appeler"
                color="#841584"
                onPress={() => Linking.openURL(`tel:${numero1}`)}>
             </Button>
            
            
            <Text>Présidente : Romane Parès</Text>
            <Text>
                {"\n"}
            </Text>
            <Button
                title="Vice-président"
                onPress={()=>{
                    Share.share({message:'Romain Bourreau 0000000000'})
                }}
                color="#841584"
                accessibilityLabel="Learn more about BDE"
            />
            <Button 
                title="Appeler"
                color="#841584"
                onPress={() => Linking.openURL(`tel:${numero2}`)}>
             </Button>
            <Text>Vice-président : Romain Bourreau</Text>
            <Text>
                {"\n"}
            </Text>
            <Button
                title="Vice-président"
                onPress={()=>{
                    Share.share({message:'Arthur Chane-Sam 0000000000'})
                }}
                color="#841584"
                accessibilityLabel="Learn more about BDE"
            />
                         <Button 
                title="Appeler"
                color="#841584"
                onPress={() => Linking.openURL(`tel:${numero3}`)}>
             </Button>
            <Text>Vice-président : Arthur Chane-Sam</Text>
            <Text>
                {"\n"}
            </Text>
            <Button
                title="Trésorière"
                onPress={()=>{
                    Share.share({message:'Victoria Mas 0000000000'})
                }}
                color="#841584"
                accessibilityLabel="Learn more about BDE"
            />
                         <Button 
                title="Appeler"
                color="#841584"
                onPress={() => Linking.openURL(`tel:${numero4}`)}>
             </Button>
            <Text>Trésorière : Victoria Mas</Text>
            <Text>
                {"\n"}
            </Text>
            <Button
                title="Respo com"
                onPress={()=>{
                    Share.share({message:'Victoire Aussudre 0000000000'})
                }}
                color="#841584"
                accessibilityLabel="Learn more about BDE"
            />
                         <Button 
                title="Appeler"
                color="#841584"
                onPress={() => Linking.openURL(`tel:${numero5}`)}>
             </Button>
            <Text>Respo com : Victoire Aussudre</Text>
            <Text>
                {"\n"}
            </Text>
            <Button
                title="Respo sponso"
                onPress={()=>{
                    Share.share({message:'Mario Pozzar 0000000000'})
                }}
                color="#841584"
                accessibilityLabel="Learn more about BDE"
            />
            <Button 
                title="Appeler"
                color="#841584"
                onPress={() => Linking.openURL(`tel:${numero6}`)}>
             </Button>
            <Text>Respo sponso : Mario Pozzar</Text>
            <Text>
                {"\n"}
            </Text>
            <Button
                title="Respo logistique"
                onPress={()=>{
                    Share.share({message:'Sonia Makzhoum 0000000000'})
                }}
                color="#841584"
                accessibilityLabel="Learn more about BDE"
            />
                         <Button 
                title="Appeler"
                color="#841584"
                onPress={() => Linking.openURL(`tel:${numero7}`)}>
             </Button>
            <Text>Respo logistique : Sonia Makzhoum</Text>
            <Text>
                {"\n"}
            </Text>
            <Button
                title="Maxou"
                onPress={()=>{
                    Share.share({message:'Maxime Beaulieu 0000000000'})
                }}
                color="#841584"
                accessibilityLabel="Learn more about BDE"
            />
                         <Button 
                title="Appeler"
                color="#841584"
                onPress={() => Linking.openURL(`tel:${numero8}`)}>
             </Button>
            <Text>Respo logistique en interim : Maxime Beaulieu</Text>
        </View>
        </ScrollView>
    );
}

/* const styles = StyleSheet.create({
    bigblue: {
    color: "black",
    fontWeight: "bold",
    fontStyle: 'italic',
    fontSize: 24,
    borderColor: "#841584",
    borderWidth: 3,
    textAlign: "center",
    backgroundColor: '#841584'
    }
    }); */

export default MembresScreen;
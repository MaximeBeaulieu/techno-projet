import React from 'react';
import { View, Text,Image,ScrollView} from 'react-native'; //LogBox,StyleSheet
import logo from './../img/logo.png';
import pull from './../img/pull.png';
import soiree from './../img/soiree.png';
import teeshirt from './../img/teeshirt.png';
import mug from './../img/mug.png';

function PrixScreen() {
    return (
        <ScrollView>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ fontSize: 24, fontWeight: '600', textAlign: "center",fontStyle: 'italic',fontWeight: 'bold'}}>{"Voici les différents prix que vous pouvez indiquer aux étudiants\n"}</Text>
           <Image source={logo} style={{width: 200, height: 200}} />
            <Text style={{ fontSize: 15, fontWeight: '600', textAlign: "center" }}>{"Adhésion 10€ pour les 1A/2A, 8€ pour les 3A/Erasmus\n"}</Text>
            <Image source={pull} style={{width: 200, height: 200}} />
            <Text style={{ fontSize: 15, fontWeight: '600', textAlign: "center" }}>{"Pull 32€\n"}</Text>
            <Image source={teeshirt} style={{width: 200, height: 200}} />
            <Text style={{ fontSize: 15, fontWeight: '600', textAlign: "center" }}>{"Tee-shirt 12€\n"}</Text>
            <Image source={mug} style={{width: 200, height: 200}} />
            <Text style={{ fontSize: 15, fontWeight: '600', textAlign: "center" }}>{"Mug + Décapsuleur 4,5€\n"}</Text>
            <Image source={soiree} style={{width: 200, height: 200}} />
            <Text style={{ fontSize: 15, fontWeight: '600', textAlign: "center" }}>{"Soirée sans alcool adhérents 5€\n"}</Text>
            <Image source={soiree} style={{width: 200, height: 200}} />
            <Text style={{ fontSize: 15, fontWeight: '600', textAlign: "center" }}>{"Soirée sans alcool non-adhérents 7€\n"}</Text>
            <Image source={soiree} style={{width: 200, height: 200}} />
            <Text style={{ fontSize: 15, fontWeight: '600', textAlign: "center" }}>{"Soirée avec alcool adhérents 8€\n"}</Text>
            <Image source={soiree} style={{width: 200, height: 200}} />
            <Text style={{ fontSize: 15, fontWeight: '600', textAlign: "center" }}>{"Soirée avec alcool non-adhérents 10€\n"}</Text>
        </View>
        </ScrollView>
    );
}

export default PrixScreen;
import React , { useState ,useEffect} from 'react';
import { View, Text ,Image,Button,FlatList,StyleSheet} from 'react-native';
import logo from './../img/logo.png';
//import './App.css';
import './eventDatabase';
import create from './eventDatabase';
import { openDatabase } from 'react-native-sqlite-storage';


var db = openDatabase({ name: 'SQLite' });


const liste_event = () => { 
  let [Evenement,setEvenement] = useState([]);
  useEffect(() => {
    db.transaction((tx) => {
        tx.executeSql(
          'SELECT * FROM evenement',
          [],
          (tx, results) => {
            var event = [];
            for (let i = 0; i < results.rows.length; ++i)
              event.push(results.rows.item(i));
            console.log(event); 
            //console.log(event[2]);
            setEvenement(event);
          }
        
        );
        //console.log(evenement)
        //console.log(Evenement)
      });
    }, []);
    return (Evenement)
}

function EvenementsScreen() {
  var event=liste_event();
  // const liste = () => {
  //    return(
  //     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
  //       <Text>Évènements </Text>
  //       <Image source={logo} style={{width: 100, height: 100}} />
  //       <FlatList
  //           data={event}
  //           renderItem={({item})=>(
  //             <Text>
  //               {item.event_id} - {item.name}
  //             </Text>)} 
  //         />
  //     </View>
  //    );
  //  }
  console.log(event);
    return (
        
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Évènements </Text>
            <Image source={logo} style={{width: 100, height: 100}} />
            
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <Button
                title="Voir la liste des évènements"
                color="#841584"
                onPress={create()}//,liste()
              />
            </View>
            <Text>
                {"\n"}
            </Text>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <Button
                title="Avoir le détail d'un évènement"
                color="#841584"
                //onPress={}
              />
            </View>
            <Text>
                {"\n"}
            </Text>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <Button
                title="Ajouter un évènement à la BDD"
                color="#841584"
                //onPress={}
              />
            </View>
            
            
            

        </View>
    );
}

export default EvenementsScreen;
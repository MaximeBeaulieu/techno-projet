import React from 'react';
import { View, Text,TouchableOpacity,Linking,Image,ScrollView } from 'react-native';
import evenements from './../img/evenements.png';
import vietudiante from './../img/vietudiante.png';


function PhotosScreen() {
    return (
        <ScrollView>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text  style={{ fontSize: 24, textAlign: "center",fontStyle: 'italic',fontWeight: 'bold'}}>Photos des différents évènements</Text>
            <Text>Les différentes photos sont disponibles sur le site d'ENSAI Studio, ci-dessous un lien vers le site, ou un lien vers les évènements plus précis en cliquant sur les photos ci-dessous.</Text>
            <Text>
                {"\n"}
            </Text>
            <TouchableOpacity onPress={() => Linking.openURL('http://ensai-studio.fr/')}>
                <Text style={{color: 'red'}}>
                    Lien direct vers le site d'ENSAI Studio 
                </Text>
                <Text>
                {"\n"}
            </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => Linking.openURL('http://ensai-studio.fr/photos-2-ensai-event.html')}>
                <Image source={evenements} style={{width: 200, height: 200}} />
            </TouchableOpacity>
            <Text>
                {"\n"}
            </Text>
            <TouchableOpacity onPress={() => Linking.openURL('http://ensai-studio.fr/photos-2-etu.html')}>
                <Image source={vietudiante} style={{width: 200, height: 200}} />
            </TouchableOpacity>
           
        </View>
         </ScrollView>
       
    );
}

export default PhotosScreen;
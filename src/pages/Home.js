import { View, Text, Button,Image,ScrollView} from 'react-native'; //LogBox,StyleSheet
import React from 'react';
import { Vibration } from "react-native";
import logo from './../img/logo.png';
// import './App.css';

function HomeScreen({ navigation }) {
    console.log("message")
    return (
        <ScrollView>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ fontSize: 24, fontWeight: '600', textAlign: "center" }}>{"Bienvenue sur l'application pour le bureau des élèves"}</Text>
            <Image source={logo} style={{width: 200, height: 200}} />
            <Button
                color="#841584"
                title="Accéder au prix"
                onPress={() => {
                    navigation.navigate('Prix');
                    Vibration.vibrate(300)
                }
                }
            />
            <Text>
                {"\n"}
            </Text>
            <Button
                color="#841584"
                title="Accéder aux événements"
                onPress={() => {
                    navigation.navigate('Evenements');
                    Vibration.vibrate(300)
                }
                }
            />
            <Text>
                {"\n"}
            </Text>
            <Button
                color="#841584"
                title="Accéder aux événements2"
                onPress={() => {
                    navigation.navigate('Evenements2');
                    Vibration.vibrate(300)
                }
                }
            />
            <Text>
                {"\n"}
            </Text>
            <Button
                color="#841584"
                title="Accéder au calendrier"
                onPress={() => {
                    navigation.navigate('Calendrier');
                    Vibration.vibrate(300)
                }
                }
            />
            <Text>
                {"\n"}
            </Text>
            <Button
                color="#841584"
                title="Accéder aux photos"
                onPress={() => {
                    navigation.navigate('Photos');
                    Vibration.vibrate(300)
                }
                }
            />
            <Text>
                {"\n"}
            </Text>
            <Button
                color="#841584"
                title="Accéder à la liste des membres"
                onPress={() => {
                    navigation.navigate('Membres');
                    Vibration.vibrate(300)
                }
                }
            />

        </View>
        </ScrollView>
    );
}

export default HomeScreen;
import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Calendar, CalendarList, Agenda, LocaleConfig } from 'react-native-calendars'; //bibliothèque pour le calendrier

LocaleConfig.locales['fr'] = {
  monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
  monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
  dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
  dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
  today: 'Aujourd\'hui'
};
LocaleConfig.defaultLocale = 'fr';

const initialDate = '2021-05-13';
const minDate = '2019-01-01';


function CalendrierScreen() {
  const [selected, setSelected] = useState(initialDate);
  const markedDates = {
    [selected]: {
      selected: true,
      disableTouchEvent: true,
      selectedColor: '#F7347A',
      selectedTextColor: 'white'
    }
  };
  const onDayPress = day => {
    setSelected(day.dateString);
  };


  return (
    <CalendarList
      current={initialDate}
      minDate={minDate}
      onDayPress={(day) => { onDayPress }}
      markedDates={markedDates}
      theme={{
        calendarBackground: '#FFC0CB',
        monthTextColor: '#515F78',
        dayTextColor: '#F7347A',
        textSectionTitleColor: '#F7347A'
      }}
      pastScrollRange={20}
      futureScrollRange={20}
      scrollEnabled={true}
      showScrollIndicator={true}
    /*
    markedDates={{
        '2021-05-16': {selected: true, marked: true, selectedColor: 'pink'},
        '2021-05-17': {marked: true},
        '2021-05-18': {marked: true, dotColor: 'red', activeOpacity: 0}
      }}
      
    */

    />);
}

export default CalendrierScreen;
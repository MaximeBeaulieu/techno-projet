/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 */

 import 'react-native-gesture-handler';
 import React from 'react';
 import Navig from './pages/Navigation';
 import {Calendar, CalendarList, Agenda, LocaleConfig} from 'react-native-calendars';
 
// import type {Node} from 'react';


const App = () => {
  return (
    <Navig />
  );
}


export default App;
